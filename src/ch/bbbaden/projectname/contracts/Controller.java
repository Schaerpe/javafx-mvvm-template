package ch.bbbaden.projectname.contracts;

public abstract class Controller {

    private Model model;

    public Controller() {
    }

    public abstract void setupDataBindings();

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

}
