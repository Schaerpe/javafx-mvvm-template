package ch.bbbaden.projectname;

import ch.bbbaden.projectname.contracts.Controller;
import ch.bbbaden.projectname.contracts.Model;
import ch.bbbaden.projectname.model.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends javafx.application.Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("view/Application.fxml"));
        Parent root = loader.load();
        Controller controller = loader.getController();
        Model model = new Application();
        controller.setModel(model);
        controller.setupDataBindings();
        primaryStage.setTitle("Mein super tolles Projekt");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
