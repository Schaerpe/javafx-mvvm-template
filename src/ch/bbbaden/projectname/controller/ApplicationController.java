package ch.bbbaden.projectname.controller;

import ch.bbbaden.projectname.contracts.Controller;
import ch.bbbaden.projectname.contracts.Model;
import javafx.fxml.FXML;
import javafx.scene.layout.GridPane;

public class ApplicationController extends Controller {

    @FXML
    private GridPane gridPane;

    public ApplicationController() {
    }

    @Override
    public void setupDataBindings() {

    }

}
